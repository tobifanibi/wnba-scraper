/* this is ready in views/js/utils.js. Needs Babel
import makeGET from '/views/js/utils.js';
*/

/* this can be imported after using Babel */
function makeGET(url, isAsync, callback) {
    /* makes a GET request to the server and executes callback, which is provided with the response text
    callback: function to execute after successful POST and response from server. Receives parsed response object
    */
    console.log('making GET to ', url);
  
    let req = new XMLHttpRequest();
    req.open('GET', url);
    req.setRequestHeader('content-type', 'application/json');
  
    // define callback for response
    if (isAsync) {
      let response;
      req.addEventListener('load', () => {
        if(req.status >= 200 && req.status < 400){
          // if a stats page wasn't available for the given name
          console.log('parsing this response text:\n' + req.responseText);
      
          response = JSON.parse(req.responseText);
  
          // generic robustness
          if (!response) {
            console.log('response couln\'t be parsed: ', req.responseText);
            alert('not working right now');
            return;
          }

          // it worked; send parsed response data to callback 
          console.log('received response:\n', response);
          callback(response);
        } else {
          // server responded with error code
          console.log("Error in network request: " + req.statusText);
          alert('not working right now');
      }});
    }
  
    // send the request
    req.send();
  }
  

  function getApiUsageData() {
    /* makes AJAX GET request to server and returns parsed results */

    // make request and define callback
    const baseUrl = window.location.hostname === 'localhost' ? 'http://localhost:3000' : '';  // hostname is automatically used in production
    const route = '/api/api-usage';
    const reqUrl = baseUrl + route;
    makeGET(reqUrl, true, (resData) => {
        displayResult(resData);
    });
}


function displayResult(resData) {
    /* displays result by filling in the two uls */

    // define text that goes in each li
    const wnbaExplanationText = ' requests received from service ';
    const nbaExplanationText = ' requests received by sister NBA API from service ';
    const noDataMessage = 'Data not available from service ';

    // make li tags for wnba data and add them to the wnba ul
    const wnbaUl = document.getElementById('wnba-req-report');
    const nbaUl = document.getElementById('nba-req-report');

    for (const servicePrefix of Object.keys(resData)) {
        // unpack data
        const nbaQueries = resData[servicePrefix].nba.queries;
        const wnbaQueries = resData[servicePrefix].wnba.queries;

        // make the nba li
        let li = document.createElement('li');
        nbaUl.appendChild(li);
        let liText;
        if (nbaQueries < 0 || typeof nbaQueries === 'undefined') {
          liText = noDataMessage + servicePrefix;
        } else {
          liText = nbaQueries + nbaExplanationText + servicePrefix;
        }
        li.textContent = liText;

        // make the wnba li
        li = document.createElement('li');
        wnbaUl.appendChild(li);
        liText;
        if (wnbaQueries < 0 || typeof wnbaQueries === 'undefined') {
          liText = noDataMessage + servicePrefix;
        } else {
          liText = wnbaQueries + wnbaExplanationText + servicePrefix;
        }
        li.textContent = liText;
    }
}


// get data just once, on page load
window.addEventListener('DOMContentLoaded', () => {
    getApiUsageData();
});
